const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    const distributorAddress = '';
    const collection = await hre.ethers.getContractAt(
        ["function setApprovalForAll(address operator, bool _approved) external"],
        "", // collection
        (await hre.ethers.getSigners())[0],
    );

    const tx = await collection.setApprovalForAll(distributorAddress, true);
    await tx.wait();
    console.log('Approved for all');
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
