const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        address: '0x11A83D28d13DC3300cE8AeD496051047Bb18a261',
    },
    bsc_testnet: {
        id: 10002,
        address: '0x04e4BFB95cEa67EceFE77F28C978B687496c9D99',
    },
    avalanche_testnet: {
        id: 10006,
        address: '0x9C083FFF8344ef2Fab3730E64624F03Da4Cd673e',
    },
    polygon_testnet: {
        id: 10009,
        address: '0x4d7DCe7E635F4f542903971f0BEf17f2Db123518',
    },
    arbitrum_testnet: {
        id: 10010,
        address: '0x31D37B30Ae511f47CC6523aca958A4D29B4577D3',
    },
    optimism_testnet: {
        id: 10011,
        address: '0x3159e20ecE0f0941859f93a158CC995F76D6B160',
    },
    ftm_testnet: {
        id: 10012,
        address: '0x140EcB5f725118E1a23e28baC2CdE727FF218523',
    },
};

async function main() {
    const contract = await hre.ethers.getContractAt('TokenFactory', '0x04e4BFB95cEa67EceFE77F28C978B687496c9D99');

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("TokenFactory: ", contract.address, ' - adding trusted remotes');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const networkParams = networkToDeployParamsMap[networkName];

        await contract.setTrustedRemote(networkParams.id, networkParams.address);
        console.log(`Set tokenFactory trusted remote for ${networkParams.id}: ${networkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });