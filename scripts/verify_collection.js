const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: "0xc88Afdd2E364e9B7f17E3E4851b45d5384456e17",
                        constructorArguments: [
                            "dad",
                            {
                                dstChainName: "Optimism",
                                name: "dada",
                                uri: "QmQFTqBZcGX514kHFq3sWVnzXaTXuusw4t39SfR42iv5vJ",
                                price: 0,
                                assetName: "USDC",
                                from: 0,
                                to: 0,
                                tokensURI: "QmVJLb8LygeLt8BCxepe7Fb99paCqLXf6pn4MGvMYgVuSr",
                                maxSupply: 14,
                                isZeroIndexed: true,
                                points: 0,
                                gas: 5000000,
                                redirectFee: 0,
                            },
                            "0xF285E70Ca2002b796A575E473285282BBf39D790",
                            "0x1217101c5B117327a79FD258821703297bDa08A0",
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 1000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
