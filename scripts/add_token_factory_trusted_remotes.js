const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'ftm_testnet';
const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        address: '0x136cc1390D5647Be6CA7790CcA3f4306d0E6d52f',
    },
    bsc_testnet: {
        id: 10002,
        address: '0xFa596C8052269F0A1A74861B75268B110A37d508',
    },
    avalanche_testnet: {
        id: 10006,
        address: '0x21bE56Cba7F118E070e200e681C04a474C567274',
    },
    polygon_testnet: {
        id: 10009,
        address: '0x9B504A662CEdD8718206c3A91397ee47D7C85f40',
    },
    arbitrum_testnet: {
        id: 10010,
        address: '0xD468f1EC8Df79A46a0EC55E9F2bD08FCF8A65498',
    },
    optimism_testnet: {
        id: 10011,
        address: '0xb1677616c1834bE0384E4b2bb28Adc92FdD8a192',
    },
    ftm_testnet: {
        id: 10012,
        address: '0x5ca02c14660a446B6538f5E06e5f14DbD903c736',
    },
};

async function main() {
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    const tokenFactory = await hre.ethers.getContractAt("TokenFactory", networkParams.address);
    const contract = await tokenFactory.deployed();

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("TokenFactory: ", contract.address, ' - adding trusted remotes');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const remoteNetworkParams = networkToDeployParamsMap[networkName];

        if (!remoteNetworkParams.address || remoteNetworkParams.address === networkParams.address) {
            console.log(`Ignoring ${networkName}`);
            continue;
        }

        await contract.setTrustedRemote(remoteNetworkParams.id, remoteNetworkParams.address);
        console.log(`Set tokenFactory trusted remote for ${remoteNetworkParams.id}: ${remoteNetworkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });