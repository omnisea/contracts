const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'mainnet';
const networkToDeployParamsMap = {
    // rinkeby: {
    //     router: '',
    //     tokenFactoryAddress: '',
    // },
    // bsc_testnet: {
    //     router: '0x6e7A5e677fD919fF4b7EeCdB19c15cA771e67DF5',
    //     tokenFactoryAddress: '0xe098952E69164174E8517805fC1Ee8874cEB22D7',
    // },
    avalanche_testnet: {
        router: '0xaA89c5E42706341b66f59B687AC2c8e90F8e6eaC',
        tokenFactoryAddress: '0xBA87579B0a51eA7a4433022C866a12Ab36341Ebe',
        pointsRepository: '0x2CD50E482DdE95F88b7686A93D73a16eb9B44C96',
    },
    polygon_testnet: {
        router: '0xAa15132a9205028f85B8bdcf52C1A3767dAA08FD',
        tokenFactoryAddress: '0xf081b7acAbbDdA6eaB8f96e31C1082898aEAB649',
        pointsRepository: '0xfC72DF636e9421BDDC103a5949b47BB08d0f6Bbb',
    },
    // arbitrum_testnet: {
    //     router: '',
    //     tokenFactoryAddress: '',
    // },
    // optimism_testnet: {
    //     router: '',
    //     tokenFactoryAddress: '',
    // },
    // ftm_testnet: {
    //     router: '',
    //     tokenFactoryAddress: '',
    // },
    // moonbeam_testnet: {
    //     router: '',
    //     tokenFactoryAddress: '',
    // },
    // zksync_testnet: {
    //     router: '0x852a4599217e76aa725f0ada8bf832a1f57a8a91',
    //     tokenFactoryAddress: '',
    // },


    // ISOLATED chains testnets:


    // MAINNET !!! //
    mainnet: {
        router: '0x1dE48d49aeBa6Ffa4740e78c84a13de8a9c12911',
        tokenFactoryAddress: '0x4cFFe7CeA9280Fe2e094E0A02BA73094f2Fb17fE',
        pointsRepository: '',
    },
    bsc_MAINNET: {
        router: '0x7E3A34C040956C6fC8B1231ab53B355998367563',
        tokenFactoryAddress: '0x37A23C498e930Af6809b23485a20A32f8f31c589',
        pointsRepository: '0x765b1ee0C4a0aFb4C8f30f4f0d0145E9cf0a11Bf',
    },
    avalanche_MAINNET: {
        router: '0xEeb51a31685bf7385B0825139320C13Dce16f5Fc',
        tokenFactoryAddress: '0x4778287dd525A8CA1d6e05aF1c71605786bED1Bf',
        pointsRepository: '',
    },
    polygon_MAINNET: {
        router: '0x077668085D7ba60832B7227E935C90C756501A0E',
        tokenFactoryAddress: '0xa4A80A9fcCC07404Ec8aC970F3BA3A91bDF42a36',
        pointsRepository: '0x034C07699A3a4e9CD61883fEfA97dC566b95CE92',
    },
    arbitrum_MAINNET: {
        router: '0xb9fd670E32B51188B9C44C2E2226Ec345F4debDc',
        tokenFactoryAddress: '0xa910545E86Ad86CceB6857F5923F17d420954c90',
        pointsRepository: '',
    },
    optimism_MAINNET: {
        router: '0x5ee0753e3aB9a991849200Ff70B342053b61ef6D',
        tokenFactoryAddress: '0x1217101c5B117327a79FD258821703297bDa08A0',
        pointsRepository: '0x3dCef56325B6C2beC7A059cfE92cdBbE3Df013c7',
    },
    ftm_MAINNET: {
        router: '0xF590d2958D557b98EF2c799813A2B35Bc1cEF4e4',
        tokenFactoryAddress: '0x5cceC7Cbdb428e3E19687D903a1adcE3C351AFdd',
        pointsRepository: '',
    },
    harmony_MAINNET: {
        router: '0xde0F6167cec79A127e6973dAa0923aD1E87b8BCC',
        tokenFactoryAddress: '0xe4c743df14c8C4f8F214C5a1F28d883D521023Af',
        pointsRepository: '',
    },
    moonbeam_MAINNET: {
        router: '0x89557E29812f1967dd40E087A9f8BA0073B5DD8A',
        tokenFactoryAddress: '0xD4c54d79CF53E5D57d3E86171feDdDA59b94f5B6',
        pointsRepository: '',
    },
    metis_MAINNET: {
        router: '0xde0F6167cec79A127e6973dAa0923aD1E87b8BCC',
        tokenFactoryAddress: '0xa7d3dB9c1FB69A59E22B7e450ca94C70c91Ef620',
        pointsRepository: '',
    },

    // ISOLATED MAINNETS:
    // evmos_MAINNET: {
    //     router: '0x20A3733909FcDA6C6B465916f5d9e2f6786075F5', // not exists yet - isolated chain
    //     tokenFactoryAddress: '',
    // },
    // gatechain_MAINNET: {
    //     router: '0x9140b8ebcd3d14f37d278c90b371f3f2b556f2ca', // not exists yet - isolated chain
    //     tokenFactoryAddress: '',
    // },
    // oasis_MAINNET: {
    //     router: '0x588BF234d86B57887d2089dea3972fE5992Df32B', // not exists yet - isolated chain
    //     tokenFactoryAddress: '',
    // },
};

async function main() {
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }

    console.log(`Deploying CollectionFactory for !!! ${network} !!!`);
    const CollectionFactory = await hre.ethers.getContractFactory("CollectionFactory");
    const collectionFactory = await CollectionFactory.deploy(networkParams.router);
    await collectionFactory.deployed();
    console.log("CollectionFactory deployed to:", collectionFactory.address);

    const CollectionsRepository = await hre.ethers.getContractFactory("CollectionsRepository");
    const collectionsRepository = await CollectionsRepository.deploy();
    await collectionsRepository.deployed();
    await (await collectionsRepository.setCollectionFactory(collectionFactory.address)).wait();
    await (await collectionsRepository.setTokenFactory(networkParams.tokenFactoryAddress)).wait();
    console.log("CollectionRepository deployed to:", collectionsRepository.address);

    await (await collectionFactory.setRepository(collectionsRepository.address)).wait();
    console.log("CollectionFactory setRepository to:", collectionsRepository.address);

    if (networkParams.pointsRepository) {
        await (await collectionFactory.setPointsRepository(networkParams.pointsRepository)).wait();
        console.log(`Set OmniseaPointsRepository at ${networkParams.pointsRepository} for ${network}`);
    }

    // If needed:
    // const collectionFactory = await hre.ethers.getContractAt(
    //     'CollectionFactory',
    //     '0x2630dc17E5258DA418dFF23C9A997628BaC2E45D',
    //     (await hre.ethers.getSigners())[0],
    // );

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: collectionFactory.address,
                        constructorArguments: [
                            networkParams.router,
                        ],
                    });

                    await hre.run("verify:verify", {
                        address: collectionsRepository.address,
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 5000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
