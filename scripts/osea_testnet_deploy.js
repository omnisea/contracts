const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    const OSEAToken = await hre.ethers.getContractFactory("OSEATokenMock");
    const token = await OSEAToken.deploy();
    await token.deployed();
    console.log("tOSEA Token deployed to:", token.address);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: token.address,
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 60000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });