const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'metis_MAINNET';
const networkToDeployParamsMap = {
    // rinkeby: {
    //     chainName: 'Ethereum',
    //     address: '0x32977c12dc7e5017a17bF60FfB718a89120d8A6B',
    // },
    // bsc_testnet: {
    //     chainName: 'BSC',
    //     address: '0xF8780698D4a5dC67c9a94F225450dB354E84e8a4',
    // },
    // avalanche_testnet: {
    //     chainName: 'Avalanche',
    //     address: '',
    // },
    // polygon_testnet: {
    //     chainName: 'Polygon',
    //     address: '',
    // },
    // arbitrum_testnet: {
    //     chainName: 'Arbitrum',
    //     address: '',
    // },
    // optimism_testnet: {
    //     chainName: 'Optimism',
    //     address: '',
    // },
    // ftm_testnet: {
    //     chainName: 'Fantom',
    //     address: '',
    // },


    // MAINNET !!! //
    mainnet: {
        chainName: 'Ethereum',
        address: '0x4cFFe7CeA9280Fe2e094E0A02BA73094f2Fb17fE',
    },
    bsc_MAINNET: { // DONE
        chainName: 'BSC',
        address: '0x37A23C498e930Af6809b23485a20A32f8f31c589',
    },
    avalanche_MAINNET: { // DONE
        chainName: 'Avalanche',
        address: '0x4778287dd525A8CA1d6e05aF1c71605786bED1Bf',
    },
    polygon_MAINNET: { // DONE
        chainName: 'Polygon',
        address: '0xa4A80A9fcCC07404Ec8aC970F3BA3A91bDF42a36',
    },
    arbitrum_MAINNET: { // DONE
        chainName: 'Arbitrum',
        address: '0xa910545E86Ad86CceB6857F5923F17d420954c90',
    },
    optimism_MAINNET: { // DONE
        chainName: 'Optimism',
        address: '0x1217101c5B117327a79FD258821703297bDa08A0',
    },
    ftm_MAINNET: { // DONE
        chainName: 'Fantom',
        address: '0x5cceC7Cbdb428e3E19687D903a1adcE3C351AFdd',
    },
    harmony_MAINNET: { // DONE
        chainName: 'Harmony',
        address: '0xe4c743df14c8C4f8F214C5a1F28d883D521023Af',
    },
    moonbeam_MAINNET: { // DONE
        chainName: 'Moonbeam',
        address: '0xD4c54d79CF53E5D57d3E86171feDdDA59b94f5B6',
    },
    metis_MAINNET: {
        chainName: 'Metis',
        address: '0xa7d3dB9c1FB69A59E22B7e450ca94C70c91Ef620',
    },
};

async function main() {
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    const contract = await hre.ethers.getContractAt('TokenFactory', networkParams.address);

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("TokenFactory: ", contract.address, ' - setting remote OA');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const remoteNetworkParams = networkToDeployParamsMap[networkName];

        if (!remoteNetworkParams.address || remoteNetworkParams.chainName === networkParams.chainName) {
            console.log(`Ignoring token factory on: ${networkName}`);
            continue;
        }
        const tx = await contract.setOA(remoteNetworkParams.chainName, remoteNetworkParams.address);

        await tx.wait();
        console.log(`Set TokenFactory remote OA for ${remoteNetworkParams.chainName}: ${remoteNetworkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
