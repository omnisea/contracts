const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        address: '0xd4E8f1D1d0AE7Cd70c513196308E3170332420e7',
    },
    bsc_testnet: {
        id: 10002,
        address: '0xD5d5B76EfEB1B4D37C494EC6F3f707F2d836d6ce',
    },
    avalanche_testnet: {
        id: 10006,
        address: '0x34D87238cf7447b959ebcD5D244c2da9F7A344c8',
    },
    polygon_testnet: {
        id: 10009,
        address: '0x0ee9D7E8d3f395801762FbA1A01a588569eF004F',
    },
    arbitrum_testnet: {
        id: 10010,
        address: '0x22571943e48BcfF31Eea47F399A649A0Ab72bd20',
    },
    optimism_testnet: {
        id: 10011,
        address: '0x094185E2B95E387127B301eB4d89F04259fB0C3a',
    },
    ftm_testnet: {
        id: 10012,
        address: '0xB5c33A3605108b887F31D42a4E12B3Ffd9756Da3',
    },
};

async function main() {
    const address = '0x22571943e48BcfF31Eea47F399A649A0Ab72bd20';
    const tokenFactory = await hre.ethers.getContractAt("CollectionFactory", address);
    const contract = await tokenFactory.deployed();

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("CollectionFactory: ", contract.address, ' - adding trusted remotes');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const networkParams = networkToDeployParamsMap[networkName];

        if (networkParams.address === null || networkParams.address === address) {
            console.log(`Ignoring token factory on: ${networkName}`);
            continue;
        }

        await contract.setTrustedRemote(networkParams.id, networkParams.address);
        console.log(`Set CollectionFactory trusted remote for ${networkParams.id}: ${networkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });