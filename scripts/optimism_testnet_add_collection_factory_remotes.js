const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        address: '0x69f86e4A90AAcB6055F2D4717942d2d32c5d92ea',
    },
    bsc_testnet: {
        id: 10002,
        address: null, // TODO: Not known yet
    },
    avalanche_testnet: {
        id: 10006,
        address: '0xa068551Eb0c45E0a5b3f1ee2B359DE28a1B17231',
    },
    polygon_testnet: {
        id: 10009,
        address: '0xBf3E649D621cb1b1FC3FdD4be1E465CBf84a793B',
    },
    arbitrum_testnet: {
        id: 10010,
        address: '0x2b4f3be774E984722cE359caeF540B13a363AD1e',
    },
    optimism_testnet: {
        id: 10011,
        address: '0xf42bd3bDe9fcA2FFc3Fe1357A6355eb8f2e36a72',
    },
    ftm_testnet: {
        id: 10012,
        address: '0x9A9fbE09D054564cD27E86Ef8F20e9B77eB7ED52',
    },
};

async function main() {
    const address = '0xf42bd3bDe9fcA2FFc3Fe1357A6355eb8f2e36a72';
    const tokenFactory = await hre.ethers.getContractAt("CollectionFactory", address);
    const contract = await tokenFactory.deployed();

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("CollectionFactory: ", contract.address, ' - adding trusted remotes');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const networkParams = networkToDeployParamsMap[networkName];

        if (networkParams.address === null || networkParams.address === address) {
            console.log(`Ignoring token factory on: ${networkName}`);
            continue;
        }

        await contract.setTrustedRemote(networkParams.id, networkParams.address);
        console.log(`Set CollectionFactory trusted remote for ${networkParams.id}: ${networkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });