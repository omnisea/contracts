const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        address: '0x8C7dd8C3143cB9d32cF35c4eD21f067Ca26dFf3B',
    },
    bsc_testnet: {
        id: 10002,
        address: '0x8C7dd8C3143cB9d32cF35c4eD21f067Ca26dFf3B', // TODO: Not known yet
    },
    avalanche_testnet: {
        id: 10006,
        address: '0x697f243aEA5fC00E386392145feaDc828D59cc7D',
    },
    polygon_testnet: {
        id: 10009,
        address: '0x36D75D651Beeb3f2D1737EE71f948A8e72b8774e',
    },
    optimism_testnet: {
        id: 10011,
        address: '0x8F5680BfF961b1BAc02C8d2566ea4DD18c0200Ca',
    },
    ftm_testnet: {
        id: 10012,
        address: '0x5704b6Ee305824261D5de572fF3Ad91e05ec63CC',
    },
};

async function main() {
    const contract = await hre.ethers.getContractAt('TokenFactory', '0xe08F3d854fe093AC6fdd5c3869d133a38F55a7eE');

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("TokenFactory: ", contract.address, ' - adding trusted remotes');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const networkParams = networkToDeployParamsMap[networkName];

        await contract.setTrustedRemote(networkParams.id, networkParams.address);
        console.log(`Set tokenFactory trusted remote for ${networkParams.id}: ${networkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });