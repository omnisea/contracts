const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'metis_MAINNET';
const networkToDeployParamsMap = {
    // rinkeby: {
    //     chainName: 'Ethereum',
    //     address: '0x9D70b5B7A2aa8905BC492A4E84a184514aE27c93',
    // },
    // bsc_testnet: {
    //     chainName: 'BSC',
    //     address: '0xe15B245CD206E05F2295BB62F42043917163Ec74',
    // },
    // avalanche_testnet: {
    //     chainName: 'Avalanche',
    //     address: '',
    // },
    // polygon_testnet: {
    //     chainName: 'Polygon',
    //     address: '',
    // },
    // arbitrum_testnet: {
    //     chainName: 'Arbitrum',
    //     address: '',
    // },
    // optimism_testnet: {
    //     chainName: 'Optimism',
    //     address: '',
    // },
    // ftm_testnet: {
    //     chainName: 'Fantom',
    //     address: '',
    // },

    // MAINNET !!! //
    mainnet: {
        chainName: 'Ethereum',
        address: '0x8CffDaECa47F5529ad0f659b2178b0e8Db401Cd8',
    },
    bsc_MAINNET: {
        chainName: 'BSC',
        address: '0x5eCA5EB1357A8309aB1C30Bb5248d3EC506033EB',
    },
    avalanche_MAINNET: {
        chainName: 'Avalanche',
        address: '0xaA3e08Cc6629361d9ceb012C5e4E4A33827299D4',
    },
    polygon_MAINNET: {
        chainName: 'Polygon',
        address: '0xc18d9449dC4D2aEAcfb89a2209C0DCd2460Ad9E5',
    },
    arbitrum_MAINNET: {
        chainName: 'Arbitrum',
        address: '0x2630dc17E5258DA418dFF23C9A997628BaC2E45D',
    },
    optimism_MAINNET: {
        chainName: 'Optimism',
        address: '0xE373AbeF6f3382Dc96FBa2f4E1c22330bA6F1dbc',
    },
    ftm_MAINNET: {
        chainName: 'Fantom',
        address: '0x8285204F6E992239c18b8993FD962BEf64cec715',
    },
    harmony_MAINNET: {
        chainName: 'Harmony',
        address: '0xf50E96c095d7d5f7185080200D59b133707D0678',
    },
    moonbeam_MAINNET: {
        chainName: 'Moonbeam',
        address: '0xfcBe3F5123d7620138459771B75088D953B9f555',
    },
    metis_MAINNET: {
        chainName: 'Metis',
        address: '0x9260beb0C74a30376d29c9A3d5216bE6e4E1E6Df',
    },
};

async function main() {
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    const contract = await hre.ethers.getContractAt('CollectionFactory', networkParams.address);

    if (!contract.address) {
        throw new Error('not deployed');
    }
    console.log("CollectionFactory: ", contract.address, ' - setting remote OA');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const remoteNetworkParams = networkToDeployParamsMap[networkName];

        if (!remoteNetworkParams.address || remoteNetworkParams.chainName === networkParams.chainName) {
            console.log(`Ignoring collection factory on: ${networkName}`);
            continue;
        }
        const tx = await contract.setOA(remoteNetworkParams.chainName, remoteNetworkParams.address);

        await tx.wait();
        console.log(`Set CollectionFactory remote OA for ${remoteNetworkParams.chainName}: ${remoteNetworkParams.address}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
