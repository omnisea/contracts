const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'mainnet';
const networkToDeployParamsMap = {
    // rinkeby: {
    //     router: '0x5262632b0b6523dD9f5eFB08098314D7750dF446',
    //     usdc: '0x15a9221776Ac71E8a5E68239c942D405646F5f3A',
    //     osea: '0x92Bf58e19e442f50Dca16c89aD10BB6e571eD2B6',
    // },
    // bsc_testnet: {
    //     router: '0x6e7A5e677fD919fF4b7EeCdB19c15cA771e67DF5',
    //     usdc: '0x3be8387A414869A1E3e567e614990b4CB1D9eC72', // 18 decimals
    //     osea: '0xEBd281FEa98908d9637cA9cc53AE80FcB437eF6e',
    //     decimals: 18,
    // },
    avalanche_testnet: {
        router: '0xaA89c5E42706341b66f59B687AC2c8e90F8e6eaC',
        usdc: '0x43ED7585E6C58eb8ebFb7aCcACAE0867Bcc6f28d',
        osea: '0xE74d210E25824173C0dE610AF50459483fbC9297',
        pointsRepository: '0x2CD50E482DdE95F88b7686A93D73a16eb9B44C96',
    },
    polygon_testnet: {
        router: '0xAa15132a9205028f85B8bdcf52C1A3767dAA08FD',
        usdc: '0x00e9e7AF1F6B586FaE6a719FCa09f00AA2dCf9DC',
        osea: '0xe6B0332DD2dAf668c624e59934BD780871feAbba',
        pointsRepository: '0xfC72DF636e9421BDDC103a5949b47BB08d0f6Bbb',
    },
    // arbitrum_testnet: {
    //     router: '',
    //     usdc: '0x00a773d8EF7391F11c0d3fA0D18A83BA9E2927e5',
    //     osea: '0x0753377F8b65a6E06C5A95EE5D86Df9E549F28bE',
    // },
    // optimism_testnet: {
    //     router: '',
    //     usdc: '0x8676FB50F6e2F9A98D08367Ee35f8e7754518c51',
    //     osea: '0x3951695Ca4e69131f50EBCcA7EfF7FDc570A7D1A',
    // },
    // ftm_testnet: {
    //     router: '',
    //     usdc: '0xbF0aB28d0e5d8ED43606AfeCB5075a17952a3340',
    //     osea: '0xb457B8334c149c54b4914fb0E4adAeF1eAead8D1',
    // },
    // moonbeam_testnet: {
    //     router: '',
    //     usdc: '0x87df883cDbcf01912AfbAd3F346C7B53CC2851fF',
    //     osea: '0xd6963e10CEa8A45C9cf45C593F89C5A8Ed8FfA08',
    // },
    zksync_testnet: {
        router: '0x852a4599217e76aa725f0ada8bf832a1f57a8a91',
        usdc: '0x852a4599217e76aa725f0ada8bf832a1f57a8a91',
        osea: '0x852a4599217e76aa725f0ada8bf832a1f57a8a91',
    },

    // !!! MAINNET NETWORKS !!!

    mainnet: {
        router: '0x1dE48d49aeBa6Ffa4740e78c84a13de8a9c12911',
        usdc: '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48',
        osea: '0xC72633F995e98Ac3BB8a89e6a9C4Af335C3D6E44',
        pointsRepository: '',
    },
    bsc_MAINNET: {
        router: '0x7E3A34C040956C6fC8B1231ab53B355998367563',
        usdc: '0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d',
        osea: '0x7b610012bDC4D6DeBa2C2D91684e408f40863429',
        pointsRepository: '0x765b1ee0C4a0aFb4C8f30f4f0d0145E9cf0a11Bf',
        decimals: 18,
    },
    avalanche_MAINNET: {
        router: '0xEeb51a31685bf7385B0825139320C13Dce16f5Fc',
        usdc: '0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E',
        osea: '0xc3e5a8A1c3B0b4c81F85Ec322554187B3575C478',
        pointsRepository: '',
    },
    polygon_MAINNET: {
        router: '0x077668085D7ba60832B7227E935C90C756501A0E',
        usdc: '0x2791bca1f2de4661ed88a30c99a7a9449aa84174',
        osea: '0xd57f8b6F3e5d1e0ab85118F5E0Dd893A08C43346',
        pointsRepository: '0x034C07699A3a4e9CD61883fEfA97dC566b95CE92',
    },
    arbitrum_MAINNET: {
        router: '0xb9fd670E32B51188B9C44C2E2226Ec345F4debDc',
        usdc: '0xff970a61a04b1ca14834a43f5de4533ebddb5cc8',
        osea: '0xC72633F995e98Ac3BB8a89e6a9C4Af335C3D6E44',
        pointsRepository: '',
    },
    optimism_MAINNET: {
        router: '0x5ee0753e3aB9a991849200Ff70B342053b61ef6D',
        usdc: '0x7f5c764cbc14f9669b88837ca1490cca17c31607',
        osea: '0xc3e5a8A1c3B0b4c81F85Ec322554187B3575C478',
        pointsRepository: '0x3dCef56325B6C2beC7A059cfE92cdBbE3Df013c7',
    },
    ftm_MAINNET: {
        router: '0xF590d2958D557b98EF2c799813A2B35Bc1cEF4e4',
        usdc: '0x04068DA6C83AFCFA0e13ba15A6696662335D5B75',
        osea: '0xC72633F995e98Ac3BB8a89e6a9C4Af335C3D6E44',
        pointsRepository: '',
    },
    harmony_MAINNET: {
        router: '0xde0F6167cec79A127e6973dAa0923aD1E87b8BCC',
        usdc: '0x985458e523db3d53125813ed68c274899e9dfab4',
        osea: '0x20035F39C6C6cB6C0d41BB88D8F443848389b809',
        pointsRepository: '',
    },
    moonbeam_MAINNET: {
        router: '0x89557E29812f1967dd40E087A9f8BA0073B5DD8A',
        usdc: '0x818ec0A7Fe18Ff94269904fCED6AE3DaE6d6dC0b',
        osea: '0x16b26B9328b1B64e4Aad6326C7cb94B2e8A96B4e',
        pointsRepository: '',
    },
    metis_MAINNET: {
        router: '0xde0F6167cec79A127e6973dAa0923aD1E87b8BCC',
        usdc: '0xea32a96608495e54156ae48931a7c20f0dcc1a21',
        osea: '0xEeb51a31685bf7385B0825139320C13Dce16f5Fc',
        pointsRepository: '',
    },

    // ISOLATED MAINNETS:
    // evmos_MAINNET: {
    //     router: '0x51e44ffad5c2b122c8b635671fcc8139dc636e82', // not exists yet - isolated chain
    //     usdc: '0x51e44ffad5c2b122c8b635671fcc8139dc636e82',
    //     osea: '0x51e44ffad5c2b122c8b635671fcc8139dc636e82', // not exists yet
    // },
    // gatechain_MAINNET: {
    //     router: '0x9140b8ebcd3d14f37d278c90b371f3f2b556f2ca', // not exists yet - isolated chain
    //     usdc: '0x9140b8ebcd3d14f37d278c90b371f3f2b556f2ca',
    //     osea: '0x9140b8ebcd3d14f37d278c90b371f3f2b556f2ca', // not exists yet
    // },
    // oasis_MAINNET: {
    //     router: '0x81ECac0D6Be0550A00FF064a4f9dd2400585FE9c', // not exists yet - isolated chain
    //     usdc: '0x81ECac0D6Be0550A00FF064a4f9dd2400585FE9c',
    //     osea: '', // not exists yet
    // },
};

async function main() {
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    console.log(`Deploying TokenFactory for !!! ${network} !!!`);
    const TokenFactory = await hre.ethers.getContractFactory("TokenFactory");
    const tokenFactory = await TokenFactory.deploy(networkParams.router);
    await tokenFactory.deployed();
    console.log("TokenFactory deployed to:", tokenFactory.address);

    await (await tokenFactory.addAsset(networkParams.usdc, 'USDC', networkParams.decimals || 6)).wait();
    console.log(`Set USDC for ${network}: ${networkParams.usdc}`);

    if (networkParams.osea) {
        await (await tokenFactory.addAsset(networkParams.osea, 'OSEA', 18)).wait();
        console.log(`Set OSEA for ${network}: ${networkParams.osea}`);
    }

    const fee = 0;
    await (await tokenFactory.setFee(fee)).wait();
    console.log(`Set ${fee} fee for ${network}`);

    if (networkParams.pointsRepository) {
        await (await tokenFactory.setPointsRepository(networkParams.pointsRepository)).wait();
        console.log(`Set OmniseaPointsRepository at ${networkParams.pointsRepository} for ${network}`);
    }

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: tokenFactory.address,
                        constructorArguments: [
                            networkParams.router,
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 45000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
