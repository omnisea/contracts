const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    console.log(`Deploying NFTDistributor`);
    const collectionAddress = '0xd348E05195a4695b5647B7e71CDe96Cc4932cB08'; // Xmas ABC

    const NFTDistributor = await hre.ethers.getContractFactory("NFTDistributor");
    const contract = await NFTDistributor.deploy(collectionAddress, 1872);
    await contract.deployed();
    console.log("NFTDistributor deployed to:", contract.address);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: contract.address,
                        constructorArguments: [
                            collectionAddress,
                            1872,
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 45000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
