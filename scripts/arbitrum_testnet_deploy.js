const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const lzEndpoint = '0x4D747149A57923Beb89f22E6B7B97f7D8c087A00';

async function main() {
    const TokenFactory = await hre.ethers.getContractFactory("TokenFactory");
    const tokenFactory = await TokenFactory.deploy(lzEndpoint);
    await tokenFactory.deployed();
    console.log("TokenFactory deployed to:", tokenFactory.address);

    const CollectionFactory = await hre.ethers.getContractFactory("CollectionFactory");
    const collectionFactory = await CollectionFactory.deploy(lzEndpoint, tokenFactory.address);
    await collectionFactory.deployed();
    console.log("CollectionFactory deployed to:", collectionFactory.address);

    const CollectionsRepository = await hre.ethers.getContractFactory("CollectionsRepository");
    const collectionsRepository = await CollectionsRepository.deploy(tokenFactory.address, collectionFactory.address);
    await collectionsRepository.deployed();
    console.log("CollectionRepository deployed to:", collectionsRepository.address);

    await tokenFactory.setChain("eth", 10001);
    await tokenFactory.setChain("bsc", 10002);
    await tokenFactory.setChain("avax", 10006);
    await tokenFactory.setChain("polygon", 10009);
    await tokenFactory.setChain("arb", 10010);
    await tokenFactory.setChain("optimism", 10011);
    await tokenFactory.setChain("ftm", 10012);
    await tokenFactory.addBaseAsset("0x00a773d8EF7391F11c0d3fA0D18A83BA9E2927e5");

    await collectionFactory.setRepository(collectionsRepository.address);
    await collectionFactory.setChain("eth", 10001);
    await collectionFactory.setChain("bsc", 10002);
    await collectionFactory.setChain("avax", 10006);
    await collectionFactory.setChain("polygon", 10009);
    await collectionFactory.setChain("arb", 10010);
    await collectionFactory.setChain("optimism", 10011);
    await collectionFactory.setChain("ftm", 10012);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: tokenFactory.address,
                        constructorArguments: [
                            lzEndpoint,
                        ],
                    });

                    await hre.run("verify:verify", {
                        address: collectionFactory.address,
                        constructorArguments: [
                            lzEndpoint,
                            tokenFactory.address,
                        ],
                    });

                    await hre.run("verify:verify", {
                        address: collectionsRepository.address,
                        constructorArguments: [
                            tokenFactory.address,
                            collectionFactory.address,
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 30000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });