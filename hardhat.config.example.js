require("@nomiclabs/hardhat-waffle");
require("dotenv").config();
require("@nomiclabs/hardhat-etherscan");
const HDWalletProvider = require("@truffle/hdwallet-provider");

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
    solidity: {
        version: "0.8.9",
        settings: {
            optimizer: {
                enabled: true,
                runs: 1,
            },
        },
    },
    paths: {
        sources: "./src/contracts",
        tests: "./test",
        cache: "./cache",
        artifacts: "./artifacts"
    },
    networks: {
        rinkeby: {
            url: "https://rinkeby.infura.io/v3/...",
            accounts: [''],
            chainId: 4,
        },
        avalanche_testnet: {
            url: 'https://api.avax-test.network/ext/bc/C/rpc',
            accounts: [''],
            chainId: 43113,
            gas: 8000000,
        },
        polygon_testnet: {
            url: 'https://matic-mumbai.chainstacklabs.com',
            accounts: [''],
            chainId: 80001,
        },
        arbitrum_testnet: {
            url: 'https://rinkeby.arbitrum.io/rpc',
            accounts: [''],
            chainId: 421611,
            gas: 100000000,
        },
        bsc_testnet: {
            url: 'https://data-seed-prebsc-2-s3.binance.org:8545',
            accounts: [''],
            chainId: 97,
        },
        ftm_testnet: {
            url: 'https://rpc.testnet.fantom.network',
            accounts: [''],
            chainId: 4002,
        },
        optimism_testnet: {
            url: 'https://kovan.optimism.io/',
            accounts: [''],
            chainId: 69,
        },
        moonbeam_testnet: {
            url: 'https://rpc.api.moonbase.moonbeam.network/',
            accounts: [''],
            chainId: 1287,
        },
    },
    etherscan: {
        apiKey: {
            rinkeby: '',
            bscTestnet: '',
            polygonMumbai: '',
            avalancheFujiTestnet: '',
            arbitrumTestnet: '',
            ftmTestnet: '',
            optimisticKovan: '',
            moonbaseAlpha: '',
        }
    }
};