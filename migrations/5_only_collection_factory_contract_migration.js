const collectionFactoryContract = artifacts.require("./CollectionFactory.sol");
const collectionsRepositoryContract = artifacts.require("./CollectionsRepository.sol");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        symbol: 'eth',
        lzEndpoint: '0x79a63d6d8BBD5c6dfc774dA79bCcD948EAcb53FA',
        tokenFactoryAddress: '0x11A83D28d13DC3300cE8AeD496051047Bb18a261',
    },
    bsc_testnet: {
        id: 10002,
        symbol: 'bsc',
        lzEndpoint: '0x6Fcb97553D41516Cb228ac03FdC8B9a0a9df04A1',
        tokenFactoryAddress: '0x04e4BFB95cEa67EceFE77F28C978B687496c9D99',
    },
    avalanche_testnet: {
        id: 10006,
        symbol: 'avax',
        lzEndpoint: '0x93f54D755A063cE7bB9e6Ac47Eccc8e33411d706',
        tokenFactoryAddress: '0xD5E4781F4BEF2516be75fCAB96bEADE002BF771e',
    },
    polygon_testnet: {
        id: 10009,
        symbol: 'polygon',
        lzEndpoint: '0xf69186dfBa60DdB133E91E9A4B5673624293d8F8',
        tokenFactoryAddress: '0xcf46e3087656438ce2c230C1Bf9ADF6172F92FAa',
    },
    arbitrum_testnet: {
        id: 10010,
        symbol: 'arb',
        lzEndpoint: '0x4D747149A57923Beb89f22E6B7B97f7D8c087A00',
        tokenFactoryAddress: '0x10EbBC6d8B2f85A3cD6C8e71C2D8629F9F7C5598',
    },
    optimism_testnet: {
        id: 10011,
        symbol: 'optimism',
        lzEndpoint: '0x72aB53a133b27Fa428ca7Dc263080807AfEc91b5',
        tokenFactoryAddress: '0xB5ea8d1335aFF6b5CdAd33abC6E12Bbe75381497',
    },
    ftm_testnet: {
        id: 10012,
        symbol: 'ftm',
        lzEndpoint: '0x7dcAD72640F835B0FA36EFD3D6d3ec902C7E5acf',
        tokenFactoryAddress: '0x140EcB5f725118E1a23e28baC2CdE727FF218523',
    },
};

module.exports = async function (deployer, network, accounts) {
    console.log(`Deploying Collection Factory on: ${network}`);
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    await deployer.deploy(collectionFactoryContract, networkParams.lzEndpoint)
    console.log(`collectionFactoryContract: ${collectionFactoryContract.address}`);
    const collectionFactoryContractInstance = await collectionFactoryContract.deployed();

    await deployer.deploy(collectionsRepositoryContract);
    console.log(`collectionsRepositoryContract: ${collectionsRepositoryContract.address}`);
    const collectionRepositoryContractInstance = await collectionsRepositoryContract.deployed();
    await collectionRepositoryContractInstance.setCollectionFactory(collectionFactoryContract.address);
    await collectionRepositoryContractInstance.setTokenFactory(networkParams.tokenFactoryAddress);
    console.log('set factories for repo');

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const remoteNetworkParams = networkToDeployParamsMap[networkName];

        await collectionFactoryContractInstance.setChain(remoteNetworkParams.symbol, remoteNetworkParams.id);
        console.log(`Set contractFactory chain id for ${remoteNetworkParams.symbol}: ${remoteNetworkParams.id}`);
    }

    await collectionFactoryContractInstance.setRepository(collectionsRepositoryContract.address);
    console.log(`Set repo for ${network}: ${collectionsRepositoryContract.address}`);
};
