const tokenFactoryContract = artifacts.require("./TokenFactory.sol");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        address: null,
    },
    bsc_testnet: {
        id: 10002,
        address: '0xe31C7d7450D96941D86C1b71541E68969076999A',
    },
    avalanche_testnet: {
        id: 10006,
        address: '0x2F261565AfBC5A74CDA71f8454986f39982872E6',
    },
    polygon_testnet: {
        id: 10009,
        address: '0x0E5F45F78eDdB3C9917245526EBCFF01C421a36F',
    },
    arbitrum_testnet: {
        id: 10010,
        address: '0xC90921360519B3A369f7e81185305E65716A7D8F',
    },
    optimism_testnet: {
        id: 10011,
        address: '0xFC01C7c9AEDC4097B7d50E68de8D5f799048137A',
    },
    ftm_testnet: {
        id: 10012,
        address: '0x74590a009dB93C4ad4d9B8d6aA01104106b29A18',
    },
};

module.exports = async function (deployer, network, accounts) {
    const contractInstance = await tokenFactoryContract.deployed();

    if (!contractInstance.address) {
        throw new Error(`${network} not deployed`);
    }
    console.log(`Setting token factories trusted remotes on ${network}`);

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }

        if (networkName === network) {
            continue;
        }
        const networkParams = networkToDeployParamsMap[networkName];

        if (networkParams.address === null) {
            console.log(`Ignoring missing token factory on: ${networkName}`);
            continue;
        }

        await contractInstance.setTrustedRemote(networkParams.id, networkParams.address);
        console.log(`Set tokenFactory trusted remote for ${networkParams.id}: ${networkParams.address}`);
    }
};
