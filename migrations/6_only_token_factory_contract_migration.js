const tokenFactoryContract = artifacts.require("./TokenFactory.sol");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        symbol: 'eth',
        lzEndpoint: '0x79a63d6d8BBD5c6dfc774dA79bCcD948EAcb53FA',
        asset: '0x15a9221776Ac71E8a5E68239c942D405646F5f3A',
    },
    bsc_testnet: {
        id: 10002,
        symbol: 'bsc',
        lzEndpoint: '0x6Fcb97553D41516Cb228ac03FdC8B9a0a9df04A1',
        asset: '0x34D87238cf7447b959ebcD5D244c2da9F7A344c8',
    },
    avalanche_testnet: {
        id: 10006,
        symbol: 'avax',
        lzEndpoint: '0x93f54D755A063cE7bB9e6Ac47Eccc8e33411d706',
        asset: '0x43ED7585E6C58eb8ebFb7aCcACAE0867Bcc6f28d',
    },
    polygon_testnet: {
        id: 10009,
        symbol: 'polygon',
        lzEndpoint: '0xf69186dfBa60DdB133E91E9A4B5673624293d8F8',
        asset: '0x4398e113FA67057c6F461ca8390b273e4148E0a1',
    },
    arbitrum_testnet: {
        id: 10010,
        symbol: 'arb',
        lzEndpoint: '0x4D747149A57923Beb89f22E6B7B97f7D8c087A00',
        asset: '0x00a773d8EF7391F11c0d3fA0D18A83BA9E2927e5',
    },
    optimism_testnet: {
        id: 10011,
        symbol: 'optimism',
        lzEndpoint: '0x72aB53a133b27Fa428ca7Dc263080807AfEc91b5',
        asset: '0x8676FB50F6e2F9A98D08367Ee35f8e7754518c51',
    },
    ftm_testnet: {
        id: 10012,
        symbol: 'ftm',
        lzEndpoint: '0x7dcAD72640F835B0FA36EFD3D6d3ec902C7E5acf',
        asset: '0xbF0aB28d0e5d8ED43606AfeCB5075a17952a3340',
    },
};

module.exports = async function (deployer, network, accounts) {
    console.log(`Deploying Token Factory on: ${network}`);
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    await deployer.deploy(tokenFactoryContract, networkParams.lzEndpoint)
    console.log(`tokenFactoryContract: ${tokenFactoryContract.address}`);
    const tokenFactoryContractInstance = await tokenFactoryContract.deployed();

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const remoteNetworkParams = networkToDeployParamsMap[networkName];

        await tokenFactoryContractInstance.setChain(remoteNetworkParams.symbol, remoteNetworkParams.id);
        console.log(`Set contractFactory chain id for ${remoteNetworkParams.symbol}: ${remoteNetworkParams.id}`);
    }

    await tokenFactoryContractInstance.addBaseAsset(networkParams.asset);
    console.log(`Set asset for ${network}: ${networkParams.asset}`);
};
