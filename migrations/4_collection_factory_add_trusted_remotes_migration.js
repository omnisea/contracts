const collectionFactoryContract = artifacts.require("./CollectionFactory.sol");

const networkToDeployParamsMap = {
    rinkeby: {
        id: 10001,
        address: '0x7A7181861Def674bF9e2cec3ff14Ab12407B8916',
    },
    bsc_testnet: {
        id: 10002,
        address: '0x667125c82F5c929dE1526078aFd53af87732c704',
    },
    avalanche_testnet: {
        id: 10006,
        address: '0x42A21357f2429D5A5ae2B99D05d3FB76a551C5d8',
    },
    polygon_testnet: {
        id: 10009,
        address: '0xD39AE6523E0e3484e316688eAAeE51Be2dA25623',
    },
    arbitrum_testnet: {
        id: 10010,
        address: '0x6E5a4AdC01557B0698345Dfe802828E5588471B7',
    },
    optimism_testnet: {
        id: 10011,
        address: '0xa8aa3B14837fF33f7914C9a469f269831D89d81a',
    },
    ftm_testnet: {
        id: 10012,
        address: '0x4D012A1aCa5b2a72341f4aC46Bb7138Ec8Bb46F8',
    },
};

module.exports = async function (deployer, network, accounts) {
    const contractInstance = await collectionFactoryContract.deployed();

    if (!contractInstance.address) {
        throw new Error(`${network} not deployed`);
    }
    console.log(`Setting token factories trusted remotes on ${network}`);

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }

        if (networkName === network) {
            continue;
        }
        const networkParams = networkToDeployParamsMap[networkName];

        if (networkParams.address === null) {
            console.log(`Ignoring missing collection factory on: ${networkName}`);
            continue;
        }

        await contractInstance.setTrustedRemote(networkParams.id, networkParams.address);
        console.log(`Set collectionFactory trusted remote for ${networkParams.id}: ${networkParams.address}`);
    }
};
