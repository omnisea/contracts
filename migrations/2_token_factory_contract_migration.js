const tokenFactoryContract = artifacts.require("./TokenFactory.sol");
const collectionFactoryContract = artifacts.require("./CollectionFactory.sol");
const collectionsRepositoryContract = artifacts.require("./CollectionsRepository.sol");

const networkToDeployParamsMap = {
    rinkeby: {
        lzEndpoint: '0x79a63d6d8BBD5c6dfc774dA79bCcD948EAcb53FA',
        symbol: 'eth',
        id: 10001,
        asset: '0x15a9221776Ac71E8a5E68239c942D405646F5f3A',
    },
    bsc_testnet: {
        lzEndpoint: '0x6Fcb97553D41516Cb228ac03FdC8B9a0a9df04A1',
        symbol: 'bsc',
        id: 10002,
        asset: '0x34D87238cf7447b959ebcD5D244c2da9F7A344c8',
    },
    avalanche_testnet: {
        lzEndpoint: '0x93f54D755A063cE7bB9e6Ac47Eccc8e33411d706',
        symbol: 'avax',
        id: 10006,
        asset: '0x43ED7585E6C58eb8ebFb7aCcACAE0867Bcc6f28d',
    },
    polygon_testnet: {
        lzEndpoint: '0xf69186dfBa60DdB133E91E9A4B5673624293d8F8',
        symbol: 'polygon',
        id: 10009,
        asset: '0x4398e113FA67057c6F461ca8390b273e4148E0a1',
    },
    arbitrum_testnet: {
        lzEndpoint: '0x4D747149A57923Beb89f22E6B7B97f7D8c087A00',
        symbol: 'arb',
        id: 10010,
        asset: '0x00a773d8EF7391F11c0d3fA0D18A83BA9E2927e5',
    },
    optimism_testnet: {
        lzEndpoint: '0x72aB53a133b27Fa428ca7Dc263080807AfEc91b5',
        symbol: 'optimism',
        id: 10011,
        asset: '0x8676FB50F6e2F9A98D08367Ee35f8e7754518c51',
    },
    ftm_testnet: {
        lzEndpoint: '0x7dcAD72640F835B0FA36EFD3D6d3ec902C7E5acf',
        symbol: 'ftm',
        id: 10012,
        asset: '0xbF0aB28d0e5d8ED43606AfeCB5075a17952a3340',
    },
}


module.exports = async function (deployer, network, accounts) {
    console.log(`Deploying: ${network}`);
    await deployer.deploy(tokenFactoryContract, networkToDeployParamsMap[network].lzEndpoint);
    console.log(`tokenFactoryContract: ${tokenFactoryContract.address}`);
    const tokenFactoryContractInstance = await tokenFactoryContract.deployed();

    await deployer.deploy(collectionFactoryContract, networkToDeployParamsMap[network].lzEndpoint, tokenFactoryContract.address)
    console.log(`collectionFactoryContract: ${collectionFactoryContract.address}`);
    const collectionFactoryContractInstance = await collectionFactoryContract.deployed();

    await deployer.deploy(collectionsRepositoryContract, tokenFactoryContract.address, collectionFactoryContract.address);
    console.log(`collectionsRepositoryContract: ${collectionsRepositoryContract.address}`);
    await collectionsRepositoryContract.deployed();

    for (let networkName in networkToDeployParamsMap) {
        if (!networkToDeployParamsMap[networkName]) {
            throw new Error(`Unknown network: ${networkName}`);
        }
        const networkParams = networkToDeployParamsMap[networkName];

        await tokenFactoryContractInstance.setChain(networkParams.symbol, networkParams.id);
        console.log(`Set tokenFactory chain id for ${networkParams.symbol}: ${networkParams.id}`);
        await collectionFactoryContractInstance.setChain(networkParams.symbol, networkParams.id);
        console.log(`Set contractFactory chain id for ${networkParams.symbol}: ${networkParams.id}`);
    }

    await tokenFactoryContractInstance.addBaseAsset(networkToDeployParamsMap[network].asset);
    console.log(`Added asset for ${network}: ${networkToDeployParamsMap[network].asset}`);
    await collectionFactoryContractInstance.setRepository(collectionsRepositoryContract.address);
    console.log(`Set repo for ${network}: ${collectionsRepositoryContract.address}`);
};
