// SPDX-License-Identifier: MIT
// omnisea-contracts v1.1

pragma solidity ^0.8.7;

import "../interfaces/ICollectionsRepository.sol";
import { CreateParams } from "../structs/erc721/ERC721Structs.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract CollectionFactory is Ownable {
    event OmReceived(string srcChain, address srcOA);

    address public repository;
    ICollectionsRepository private _collectionsRepository;

    function setRepository(address repo) external onlyOwner {
        _collectionsRepository = ICollectionsRepository(repo);
        repository = repo;
    }

    function create(CreateParams calldata params) public payable {
        require(bytes(params.name).length >= 2);
        _collectionsRepository.create(params, msg.sender);
    }

    receive() external payable {}
}
