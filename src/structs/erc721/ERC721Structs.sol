// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

struct CreateParams {
    string dstChainName;
    string name;
    string uri;
    uint256 price;
    string assetName;
    uint256 from;
    uint256 to;
    string tokensURI;
    uint256 maxSupply;
    bool isZeroIndexed;
    uint24 royaltyAmount;
}

struct MintParams {
    address coll;
    uint256 quantity;
    bytes32[] merkleProof;
}

struct Allowlist {
    uint256 maxPerAddress;
    uint256 maxPerAddressPublic;
    uint256 publicFrom;
    uint256 price;
    bytes32 merkleRoot;
    bool isEnabled;
}
